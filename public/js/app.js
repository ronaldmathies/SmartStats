'use strict';

var moduleId = "SmartStatsApp";

var app = angular.module(moduleId, ['ui.router', 'ngMaterial', 'chart.js'])
    .config(function($mdThemingProvider, $urlRouterProvider, $stateProvider, $locationProvider) {
        $mdThemingProvider.theme('docs-dark', 'default')
            .primaryPalette('green')
            .warnPalette('red')
            .dark();

        $stateProvider
            .state("home", {
                url: "/home",
                templateUrl: "partials/home.html"
            })
            .state("config", {
                url: "/config",
                templateUrl: "partials/config.html"
            })

        $urlRouterProvider.otherwise("/");
        $locationProvider.html5Mode({enabled: true, requireBase: false});
    })
    .run(function() {
        // Do not maintain aspect ratio width / height.
        Chart.defaults.global.maintainAspectRatio = false;
        console.log('SmartStatsApp is ready.')
    });
