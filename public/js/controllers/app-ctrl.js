'use strict';

angular.module(moduleId).controller('appCtrl', function($scope, $mdSidenav) {

    $scope.toggleMenu = function(componentId) {
        $mdSidenav(componentId).toggle();
    }
});