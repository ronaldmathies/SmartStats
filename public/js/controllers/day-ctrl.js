'use strict';

angular.module(moduleId).controller('dayGraphCtrl', function($scope, $timeout, $mdBottomSheet) {
    $scope.labels = ["10:12:10", "10:12:20", "10:12:30", "10:12:40", "10:12:50", "10:12:50", "10:13:00"];
    $scope.series = ['Watt'];
    $scope.data = [
        [400, 410, 420, 390, 380, 400, 440]
    ];
    $scope.onClick = function (points, evt) {
        console.log(points, evt);
    };
    $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
    $scope.options = {
        scales: {
            yAxes: [
                {
                    id: 'y-axis-1',
                    type: 'linear',
                    display: true,
                    position: 'left'
                }
            ],
            legend: {
                display: true,
                labels: {
                    fontColor: 'rgb(255, 99, 132)'
                }
            }
        }
    };

    $scope.reload = function() {

        // TODO: Replace with call to endpoint or use web-sockets.
        $scope.data[0].shift();
        $scope.data[0].push(Math.floor(Math.random() * 400) + 300);

        $timeout(function() {
            $scope.reload();
        }, 5000);
    };

    $scope.settings = function(event) {
        $mdBottomSheet.show({
            templateUrl: '/templates/day-settings-template.html',
            controller: 'daySettingsCtrl'
        }).then(function(criteria) {

        });
    };

    $scope.reload();
});

angular.module(moduleId).controller('daySettingsCtrl', function($scope, $mdBottomSheet) {
    $scope.save = function() {
        $mdBottomSheet.hide({});
    };
});