'use strict';

const logging = require('bunyan'),
      mongoClient = require('mongodb').MongoClient,
      smconfig = require('../config/sm-config');

var db;

var log = logging.createLogger({name: 'sm-db'});

exports.connect = function(onSuccess, onFailure) {
    var url = 'mongodb://' + smconfig.getMongoDbHost() + ':' + smconfig.getMongoDbPort() + '/' + smconfig.getMongoDbSchema();

    log.info('Connecting with mongo-db using "%s"', url)
    mongoClient.connect('mongodb://' + smconfig.getMongoDbHost() + ':' + smconfig.getMongoDbPort() + '/' + smconfig.getMongoDbSchema())
        .then(function (_db) { // <- db as first argument
            log.info("Connection with mongo-db successful connected");
            db = _db;
            onSuccess();
        })
        .catch(function (err) {
            log.error('Connection with mongo-db unsuccesful: "%s" ', err.stack);
            onFailure();
        })
};

exports.disconnect = function(onSuccess) {
    db.close(true);
    log.info('Connection with mongo-db successful disconnected.');
    onSuccess();
};