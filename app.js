'use strict';

const smdb     = require('./db/sm-db'),
      smconfig = require('./config/sm-config'),
      logging  = require('bunyan'),
      restify  = require('restify'),
      restifyValidator = require('restify-validator'),
      fs       = require('fs');

var log = logging.createLogger({
    name: 'app',
    stream: process.stdout
});

var server = restify.createServer({});

server.use(restify.bodyParser());
server.use(restify.queryParser());
server.use(restifyValidator);

server.on('after', restify.auditLogger({
    log: log
}));


server.get(/^\/js/, restify.serveStatic({ directory: './public/' }));
server.get(/^\/css/, restify.serveStatic({ directory: './public/' }));
server.get(/^\/lib/, restify.serveStatic({ directory: './public/' }));
server.get(/^\/partials/, restify.serveStatic({ directory: './public/' }));
server.get(/^\/templates/, restify.serveStatic({ directory: './public/' }));

// Alle content exposen voor de browser behalve /api/xxx operaties.
server.get(/^(?!\/api\/).+/, function(request, response, next) {
    fs.readFile('./public/index.html', function(err, file) {
        response.write(file);
        response.end();
        return next();
    });
});

server.get('/api/v1/statistics', getStatistics);

function getStatistics(request, response, next) {
    response.send({
        count: 1000
    });
    next();
}

smconfig.loadAndValidateConfig(function() {
    smdb.connect(function() {
        server.listen(smconfig.getHttpPort(), function() {
            log.info('SmartStats is listening on port %d.', smconfig.getHttpPort());
        });
    }, function() {
        process.exit();
    });
})

process.on('SIGTERM', function() {
    smdb.disconnect(function() {
       log.info('SmartStats has been shutdown.');
       process.exit();
    });
});

process.on('SIGINT', function() {
    smdb.disconnect(function() {
        log.info('SmartStats has been shutdown.');
        process.exit();
    });
});