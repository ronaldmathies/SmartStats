'use strict';

const logging = require('bunyan'),
      nconf = require('nconf'),
      fs = require('fs');

var log = logging.createLogger({name: 'sm-config'});


exports.loadAndValidateConfig = function(onSuccess) {
    var environment = process.env.NODE_ENV;
    log.info('Environment: ' + environment);

    var configFile = './config/' + environment + '.config';
    if (!fs.existsSync(configFile)) {
        log.info('config file ' + configFile + ' not found.');
    }

    nconf.argv()
        .env()
        .file({
            file: configFile
        });

    onSuccess();
}

exports.getHttpPort = function() {
    return nconf.get("http:port");
}

exports.getMongoDbHost = function() {
    return nconf.get("mongodb:host");
}

exports.getMongoDbPort = function() {
    return nconf.get("mongodb:port");
}

exports.getMongoDbSchema = function() {
    return nconf.get("mongodb:schema");
}

